package forms;

import org.gradoservice.mapRender.layers.Layer;

/**
 * Created with IntelliJ IDEA.
 * User: germanosin
 * Date: 15.06.14
 * Time: 18:04
 * To change this template use File | Settings | File Templates.
 */
public interface ILayerRequest {
    public Layer getLayer();
}
